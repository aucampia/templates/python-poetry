# ...

```bash
poetry new --src --name python-poetry python-poetry
mv -v README.{rst,md}
poetry add --dev mypy pylint
```

```bash
dasel select -f pyproject.toml -m 'tool.poetry.dev-dependencies.-' | sed 's/.*/&@latest/g' | xargs -n1 echo poetry add --dev
dasel select -f pyproject.toml -m 'tool.poetry.dependencies.-' | grep -v '^python' | sed 's/.*/&@latest/g' | xargs -n1 echo poetry add

dasel select -f pyproject.toml -m 'tool.poetry.dev-dependencies.-' | sed 's/.*/&@latest/g' | xargs -n1 -t poetry add --dev
dasel select -f pyproject.toml -m 'tool.poetry.dependencies.-' | grep -v '^python' | sed 's/.*/&@latest/g' | xargs -n1 -t poetry add
```


## TODO:

- poetry-dynamic-versioning
