# python-poetry template

## Using

```bash
## Initial sync
rsync -av --no-owner --no-group --no-times --checksum --no-perms --ignore-existing \
    --exclude={.git,TEMPLATE-*.md,poetry.lock,__pycache__,*.egg-info,.pytest_cache,.mypy_cache,.venv,.tox,setup.py,.cache-*,dist,.coverage,coverage.xml,extra,LICENSE} \
    ~/sw/d/gitlab.com/aucampia/templates/python-poetry/ ./ --dry-run

## Diff summary ...
diff -u -r -q \
    --exclude={.git,TEMPLATE-*.md,poetry.lock,__pycache__,*.egg-info,.pytest_cache,.mypy_cache,.venv,.tox,setup.py,.cache-*,dist,.coverage,coverage.xml,extra,LICENSE} \
    ~/sw/d/gitlab.com/aucampia/templates/python-poetry/ ./

## vimdiff
diff -u -r \
    --exclude={.git,TEMPLATE-*.md,poetry.lock,__pycache__,*.egg-info,.pytest_cache,.mypy_cache,.venv,.tox,setup.py,.cache-*,dist,.coverage,coverage.xml,extra,LICENSE} \
    ~/sw/d/gitlab.com/aucampia/templates/python-poetry/ ./ \
    | sed -E -n 's,^diff.* /,vimdiff /,gp'

## diff
diff -u -r \
    --exclude={.git,TEMPLATE-*.md,poetry.lock,__pycache__,*.egg-info,.pytest_cache,.mypy_cache,.venv,.tox,setup.py,.cache-*,dist,.coverage,coverage.xml,extra,LICENSE} \
    ~/sw/d/gitlab.com/aucampia/templates/python-poetry/ ./
```

MARK 000
